package com.eonics.reactiveworkshop.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Burger {
    String id;
}
