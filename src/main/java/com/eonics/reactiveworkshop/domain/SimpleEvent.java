package com.eonics.reactiveworkshop.domain;

import lombok.Data;

@Data
public class SimpleEvent {
    private String orderId;
}
