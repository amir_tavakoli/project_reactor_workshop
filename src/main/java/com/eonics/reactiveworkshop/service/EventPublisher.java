package com.eonics.reactiveworkshop.service;

import com.eonics.reactiveworkshop.domain.SimpleEvent;
import reactor.core.publisher.Mono;

public class EventPublisher {

    public Mono<String> publishCompleted(final SimpleEvent toPublish) {
        return Mono.just(toPublish)
                   .doOnNext(event -> System.out.println("EventPublisher::publishCompleted"))
                   .map(event -> "Succeeded");
    }

    public Mono<Void> publishReturningMonoVoid(final SimpleEvent toPublish) {
        return Mono.just(toPublish)
                   .doOnNext(event -> System.out.println("EventPublisher::publishReturningMonoVoid"))
                   .then();
    }
}
