package com.eonics.reactiveworkshop.service;

import com.eonics.reactiveworkshop.domain.BurgerResponse;
import com.eonics.reactiveworkshop.domain.SimpleEvent;

public class SimpleEventAdapter {

    public SimpleEvent adapt(final BurgerResponse burgerResponse) {
        System.out.println("SimpleEventAdapter::adapt");
        return new SimpleEvent();
    }
}
