package com.eonics.reactiveworkshop.service;

import com.eonics.reactiveworkshop.domain.BurgerRequest;
import com.eonics.reactiveworkshop.domain.Burger;

public class BurgerRequestAdapter {

    public BurgerRequest adapt(final Burger burger) {
        System.out.println("BurgerRequestAdapter::adapt");
        return new BurgerRequest();
    }

    public BurgerRequest adaptErrorProne(final Burger burger) {
        System.out.println("BurgerRequestAdapter::adaptErrorPrune");
        if(true) {
            throw new RuntimeException("BurgerRequestAdapterErrorMessage");
        }
        return new BurgerRequest();
    }

    public BurgerRequest adaptErrorProneOnId1(final Burger burger) {
        System.out.println("BurgerRequestAdapter::adaptErrorPrune");
        if(burger.getId().equals("1")) {
            throw new RuntimeException("BurgerRequestAdapterErrorMessage");
        }
        return new BurgerRequest();
    }

    public BurgerRequest adaptErrorProneOnId2(final Burger burger) {
        System.out.println("BurgerRequestAdapter::adaptErrorPrune");
        if(burger.getId().equals("2")) {
            throw new RuntimeException("BurgerRequestAdapterErrorMessage");
        }
        return new BurgerRequest();
    }
}
