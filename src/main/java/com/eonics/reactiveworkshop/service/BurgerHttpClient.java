package com.eonics.reactiveworkshop.service;

import com.eonics.reactiveworkshop.domain.BurgerRequest;
import com.eonics.reactiveworkshop.domain.BurgerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


public class BurgerHttpClient {

    public Mono<BurgerResponse> book(final BurgerRequest burgerRequest) {

        return Mono.just(burgerRequest)
                   .doOnNext(request -> System.out.println("BurgerClient::book"))
                   .map(request -> new BurgerResponse());
    }

    public Mono<BurgerResponse> bookErrorPrune(final BurgerRequest burgerRequest) {

        System.out.println("BurgerClient::bookErrorPrune");
        throw new RuntimeException("errorMessage");
    }

    public Mono<BurgerResponse> bookReturningEmptyMono(final BurgerRequest burgerRequest) {

        System.out.println("BurgerClient::bookReturningEmptyMono");
        return Mono.empty();
    }

    public Flux<String> bookReturningListOfIds(final BurgerRequest burgerRequest) {

        System.out.println("BurgerClient::bookReturningListOfIds");
        return Flux.fromIterable(List.of("1","2","3","4"));
    }

    public Mono<String> cancel() {
        System.out.println("BurgerClient::cancel");
        return Mono.just("Canceled");
    }
}
