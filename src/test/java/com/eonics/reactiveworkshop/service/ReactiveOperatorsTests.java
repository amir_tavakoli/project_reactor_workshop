package com.eonics.reactiveworkshop.service;

import com.eonics.reactiveworkshop.domain.Burger;
import com.eonics.reactiveworkshop.domain.BurgerRequest;
import com.eonics.reactiveworkshop.domain.BurgerResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ReactiveOperatorsTests {

    Burger burgerObj = Burger.builder().id("id1").build();
    BurgerRequestAdapter burgerRequestAdapter = new BurgerRequestAdapter();
    BurgerHttpClient burgerHttpClient = new BurgerHttpClient();
    SimpleEventAdapter simpleEventAdapter = new SimpleEventAdapter();
    EventPublisher eventPublisher = new EventPublisher();
    ConfigurationClient configurationClient = new ConfigurationClient();

    @Test
    void mapVsFlatMap() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();

    }

    @Test
    void methodReference() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burgerRequestAdapter::adapt)
                                .flatMap(burgerHttpClient::book)
                                .map(simpleEventAdapter::adapt)
                                .flatMap(eventPublisher::publishCompleted);

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();

    }

    @Test
    void changingSignalToEmptyUsingFilter() {

        Mono<String> mono = Mono.just(burgerObj)
                                .filter(burger -> burger.getId().equals("bid2"))
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .verifyComplete();

    }

    @Test
    void changingSignalToEmptyUsingEmptyMono() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.bookReturningEmptyMono(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .verifyComplete();

    }

    @Test
    void changingSignalToEmptyUsingReactiveBoolean() {

        Mono<String> mono = Mono.just(burgerObj)
                                .filterWhen(burger -> configurationClient.isSupport(burger))
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.bookReturningEmptyMono(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .verifyComplete();

    }

    //Mono<Void> == Mono.empty()
    @Test
    void changingSignalToEmptyUsingMonoVoid() {

        Mono<Void> mono = Mono.just(burgerObj)
                              .map(burger -> burgerRequestAdapter.adapt(burger))
                              .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                              .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                              .flatMap(simpleEvent -> eventPublisher.publishReturningMonoVoid(simpleEvent));

        StepVerifier.create(mono)
                    .verifyComplete();

    }


    @Test
    void handlingEmptySignal() {

        Mono<String> mono = Mono.just(burgerObj)
                                .filter(burger -> burger.getId().equals("bid2"))
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .switchIfEmpty(burgerHttpClient.cancel());

        StepVerifier.create(mono)
                    .expectNext("Canceled")
                    .verifyComplete();

    }

    @Test
    void handlingEmptySignalUsingNoneLazyMono() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .switchIfEmpty(burgerHttpClient.cancel());

        //The codes that are out of the pipeline of BurgerClient::cancel is being called
        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void handlingEmptySignalShouldBeLazy() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .switchIfEmpty(Mono.defer(() -> burgerHttpClient.cancel()));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void handlingEmptySignalWithValue() {

        Mono<String> mono = Mono.just(burgerObj)
                                .filter(burger -> burger.getId().equals("bid2"))
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .defaultIfEmpty("Default");

        StepVerifier.create(mono)
                    .expectNext("Default")
                    .verifyComplete();
    }

    @Test
    void changingSignalToError() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }

    @Test
    void whenExceptionHappensOutsideOfPipeline() {

        Executable executable = () -> Mono.just(burgerRequestAdapter.adaptErrorProne(burgerObj))
                                          .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                          .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                          .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        assertThrows(RuntimeException.class, executable, "BurgerRequestAdapterErrorMessage");
    }

    @Test
    void startPipelineUsingNoneReactiveMethod() {

        Mono<String> mono = Mono.fromCallable(() -> burgerRequestAdapter.adaptErrorProne(burgerObj))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");

    }

    @Test
    void recoveryFromErrorSignal1() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .onErrorResume(exception -> Mono.just(new BurgerRequest())) //The handler should return Mono<BurgerRequest>
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void recoveryFromErrorSignal2() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .onErrorResume(exception -> Mono.just(new BurgerResponse())) //The handler should return Mono<BurgerResponse>
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void usingDoOnSuccess1() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .doOnSuccess(string -> System.out.println("doOnSuccess"));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }


    @Test
    void usingDoOnSuccess2() {

        Mono<String> mono = Mono.just(burgerObj)
                                .doOnSuccess(request -> System.out.println("doOnSuccess"))
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        //DoOnSuccess is being executed
        StepVerifier.create(mono)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }

    @Test
    void usingDoOnError1() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .doOnError(exception -> System.out.println("doOnError"));

        StepVerifier.create(mono)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }


    @Test
    void usingDoOnError2() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .doOnError(exception -> System.out.println("doOnError1"))
                                .onErrorResume(exception -> Mono.just(new BurgerRequest()))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .doOnError(exception -> System.out.println("doOnError2"));

        StepVerifier.create(mono)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void passingMonoToDoOnError() {

        Mono<String> mono = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent))
                                .doOnError(exception -> System.out.println("doOnError1"))
                                .doOnError(exception -> doOnError2().subscribe());

        StepVerifier.create(mono)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }

    private Mono<String> doOnError2() {
        return Mono.just("doOnError2")
                   .doOnNext(string -> System.out.println(string)); //This line will not be executed until the mono is subscribed
    }

    @Test
    void usingFlux1() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNext("Succeeded")
                    .expectNext("Succeeded")
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void usingFlux2() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNextCount(3)
                    .verifyComplete();
    }

    @Test
    void usingOneErrorSignal() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adaptErrorProne(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }

    @Test
    void mixErrorAndSignal() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adaptErrorProneOnId2(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNext("Succeeded")
                    .verifyErrorMessage("BurgerRequestAdapterErrorMessage");
    }

    @Test
    void useFilter() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .filter(burger -> !burger.getId().equals("2"))
                                .map(burger -> burgerRequestAdapter.adaptErrorProneOnId2(burger))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNext("Succeeded")
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void recoveryFromError() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adaptErrorProneOnId1(burger))

                                .onErrorResume(e -> Mono.just(new BurgerRequest()))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void recoveryFromError2() {

        Burger burgerObj1 = Burger.builder().id("1").build();
        Burger burgerObj2 = Burger.builder().id("2").build();
        Burger burgerObj3 = Burger.builder().id("3").build();
        List<Burger> burgers = List.of(burgerObj1, burgerObj2, burgerObj3);

        Flux<String> flux = Flux.fromIterable(burgers)
                                .map(burger -> burgerRequestAdapter.adaptErrorProneOnId1(burger))
                                .onErrorContinue((throwable, burger) -> System.out.println("error in: " + burger.toString()))
                                .flatMap(burgerRequest -> burgerHttpClient.book(burgerRequest))
                                .map(burgerResponse -> simpleEventAdapter.adapt(burgerResponse))
                                .flatMap(simpleEvent -> eventPublisher.publishCompleted(simpleEvent));

        StepVerifier.create(flux)
                    .expectNext("Succeeded")
                    .expectNext("Succeeded")
                    .verifyComplete();
    }

    @Test
    void changingFromMonoToFlux() {

        Flux<String> flux = Mono.just(burgerObj)
                                .map(burger -> burgerRequestAdapter.adapt(burger))
                                .flatMapMany(burgerRequest -> burgerHttpClient.bookReturningListOfIds(burgerRequest))
                                .doOnNext(item -> System.out.println(item));

        StepVerifier.create(flux)
                    .expectNext("1")
                    .expectNext("2")
                    .expectNext("3")
                    .expectNext("4")
                    .verifyComplete();
    }
}
